#include <map>
#include <string>
#include <vector>
#include <algorithm>

// IOT socket api
#include <iot/socket.hpp>

#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <chat.hpp>

#define USER_ALL "__ALL"
#define USER_END "END"



struct Group {
    std::string name;
    std::vector<std::string> members;
    std::map<std::string, sockaddr_in*> user_info;

};

/**
 * @brief map of current online clients
*/
typedef std::map<std::string, sockaddr_in *> online_users;
std::map<std::string, Group> groups;

// void handle_list(
//     online_users& online_users, std::string username, std::string,
//     struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop);

/**
 * @brief Send a given message to all clients
 *
 * @param msg to send
 * @param username used if  not to send to that particular user
 * @param online_users current online users
 * @param sock socket for communicting with client 
 * @param send_to_username determines also to send to username
*/
void send_all(
    chat::chat_message& msg, std::string username, online_users& online_users, 
    uwe::socket& sock, bool send_to_username = true) {
    for (const auto user: online_users) {    
        if ((send_to_username && user.first.compare(username) == 0) || user.first.compare(username) != 0) { 
            int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                (sockaddr*)user.second, sizeof(struct sockaddr_in));
        }
    }   
}

bool create_group(const std::string& group_name) {

    DEBUG("Creation started\n");
    // Check if the group already exists
    if (groups.find(group_name) == groups.end()) {
         DEBUG("Creation continued\n");
        // Create a new group and add it to the map
        Group new_group;
        new_group.name = group_name;
        groups[group_name] = new_group;
        // Optionally, you can log or print a message indicating the group creation
         DEBUG("Creation stopped\n");
         return true; // Group created successfully
    } else {
        // Group already exists, stop the creation process and log a debug message
        DEBUG("Group %s already exists.\n", group_name.c_str());
        return false; // Group creation failed
    }
}

void handle_group_creation(
    online_users& online_users, std::string, std::string, std::string group_name, struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    std::string creator_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            creator_username = user.first;
            break;
        }
    }

   if (!create_group(group_name)) {
        return;
    }

    DEBUG("Group %s was created \n", group_name.c_str());

    Group& group = groups[group_name];
    //creator added to group as a member
    group.members.push_back(creator_username);

    group.user_info[creator_username] = &client_address;

    auto msg = chat::create_group_msg(group_name);
    int len = sock.sendto(
        reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
        (sockaddr*)&client_address, sizeof(struct sockaddr_in));
    // Display message in the UI indicating group creation
    // This depends on how the UI is structured and updated
}

void handle_user_addition_to_group(
    online_users& online_users, std::string username, std::string, std::string group_name, struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    std::string sender_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            sender_username = user.first;
            break;
        }
    }

    Group& group = groups[group_name];
    if (std::find(group.members.begin(), group.members.end(), sender_username) != group.members.end()) {
        // User is already in the group, handle error
        DEBUG("User %s is already in group %s\n", sender_username.c_str(), group_name.c_str());

        // Check if the group exists
        if (groups.find(group_name) == groups.end()) {
            // Group does not exist, handle error
            DEBUG("Group %s does not exist\n", group_name.c_str());
        // TODO: Handle error message
        return;
        }
    
        // Check if the user is already in the group
        if (std::find(group.members.begin(), group.members.end(), username) != group.members.end()) {
            // User is already in the group, handle error
            DEBUG("User %s is already in group %s\n", username.c_str(), group_name.c_str());
            // TODO: Handle error message
            return;
        }
    
        // Add user to the group
        group.members.push_back(username);

        group.user_info[username] = &client_address;
    
        // Notify the user about being added to the group
        if (auto search = online_users.find(username); search != online_users.end()) {
            // Retrieve the recipient's socket address
            sockaddr_in* recipient_addr = search->second;
            // Send the message to the recipient
            auto msg = chat::add_user_to_group_msg(group_name, username);
            DEBUG("User %s was added to group %s \n", username.c_str(), group_name.c_str());
            int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                reinterpret_cast<sockaddr*>(recipient_addr), sizeof(struct sockaddr_in));
            
            // Check if message sent successfully
            if (len == sizeof(chat::chat_message)) {
                DEBUG("Alert sent to %s\n", username.c_str());
            } else {
                // Handle error if message sending failed
                DEBUG("Failed to send alert to %s\n", username.c_str());
                // TODO: Handle error message
            }
        } else {
            // TODO: Handle error message
            DEBUG("User %s not found\n", username.c_str());
        }
    } else{
        DEBUG("User not a member");
        }
}

void handle_user_removal_from_group(
    online_users& online_users, std::string username, std::string, std::string group_name, struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    std::string sender_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            sender_username = user.first;
            break;
        }
    }
    auto group_iter = groups.find(group_name);
    if (group_iter != groups.end()) {
        // Group found, remove the user from the group's member list
        auto& group = group_iter->second;
        auto it = std::find(group.members.begin(), group.members.end(), username);
        if (it != group.members.end()) {
            group.members.erase(it);

            if (auto search = online_users.find(username); search != online_users.end()) {
                // Retrieve the recipient's socket address
                sockaddr_in* recipient_addr = search->second;
                auto msg = chat::remove_user_from_group_msg(group_name, sender_username);
                int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                reinterpret_cast<sockaddr*>(recipient_addr), sizeof(struct sockaddr_in));
                
                DEBUG("User %s was removed from group %s\n", username.c_str(), group_name.c_str());
            }

        } else {
            DEBUG("User %s not found in group %s\n", username.c_str(), group_name.c_str());
        }
    } else {
        DEBUG("Group %s not found\n", group_name.c_str());
    }
    
}


void handle_user_leave_group(
    online_users& online_users, std::string username, std::string, std::string group_name,  struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    std::string sender_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            sender_username = user.first;
            break;
        }
    }

    // Check if the group exists
    if (groups.find(group_name) == groups.end()) {
        // Group does not exist, handle error
        DEBUG("Group %s does not exist\n", group_name.c_str());
        // TODO: Handle error message
        return;
    }
    
    // Retrieve the group information
    Group& group = groups[group_name];
    
    // Remove the user from the group
    auto it = std::find(group.members.begin(), group.members.end(), sender_username);
    if (it != group.members.end()) {
        group.members.erase(it);
    } else {
        // User is not a member of the group, handle error
        DEBUG("User %s is not a member of group %s\n", username.c_str(), group_name.c_str());
        // TODO: Handle error message
        return;
    }
    
    // Notify all group members about the user leaving
    for (const auto& member : group.members) {
        if (auto search = online_users.find(member); search != online_users.end()) {
            // Retrieve the recipient's socket address
            sockaddr_in* recipient_addr = search->second;
            // Send the message to the recipient
            auto msg = chat::leave_group_msg(group_name, sender_username );
            int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                reinterpret_cast<sockaddr*>(recipient_addr), sizeof(struct sockaddr_in));
            
            int len_2 = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                (sockaddr*)&client_address, sizeof(struct sockaddr_in));

            // Check if message sent successfully
            if (len && len_2 != sizeof(chat::chat_message)) {
                // Handle error if message sending failed
                DEBUG("Failed to send group leave message to %s\n", member.c_str());
                // TODO: Handle error message
            }
        } else {
            // TODO: Handle error message
            DEBUG("User %s not found\n", member.c_str());
        }
    }
    
    // Display message indicating user leaving the group
    DEBUG("%s left group %s\n", sender_username.c_str(), group_name.c_str());
    }

void handle_group_message(
    online_users& online_users, std::string, std::string message, std::string group_name, struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    // Find the sender's username based on the client address
    std::string sender_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            sender_username = user.first;
            break;
        }
    }

    // Check if the sender's username was found
    if (sender_username.empty()) {
        // Handle error: sender's username not found
        DEBUG("Sender's username not found for client address\n");
        return;
    }

    DEBUG("User %s sent message '%s' to group %s\n", sender_username.c_str(), message.c_str(), group_name.c_str());

    // Check if the group exists
    auto group_it = groups.find(group_name);
    if (group_it == groups.end()) {
        // Group does not exist, handle error
        DEBUG("Group %s does not exist\n", group_name.c_str());
        // TODO: Handle error message
        return;
    }

    // Check if the sender is a member of the group
    Group& group = group_it->second;
    if (std::find(group.members.begin(), group.members.end(), sender_username) == group.members.end()) {
        // Sender is not a member of the group, handle error
        DEBUG("User %s is not a member of group %s\n", sender_username.c_str(), group_name.c_str());
        // TODO: Handle error message
        return;
    }

    // Iterate over group members to send the message
    for (const std::string& member_username : group.members) {
        // Skip sending the message to the sender
        if (member_username == sender_username) {
            continue;
        }

        // Find the recipient's socket address
        auto recipient_it = online_users.find(member_username);
        if (recipient_it != online_users.end()) {
            // Retrieve the recipient's socket address
            sockaddr_in* recipient_addr = recipient_it->second;

            // Send the message to the recipient
            auto msg = chat::group_msg(group_name, sender_username, message);
            int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                reinterpret_cast<sockaddr*>(recipient_addr), sizeof(struct sockaddr_in));

            // Check if message sent successfully
            if (len != sizeof(chat::chat_message)) {
                // Handle error if message sending failed
                DEBUG("Failed to send message to user %s in group %s\n", member_username.c_str(), group_name.c_str());
                // TODO: Handle error message
            }
        } else {
            // Handle error if recipient not found
            DEBUG("User %s not found in online users\n", member_username.c_str());
            // TODO: Handle error message
        }
    }
}



/**
 * @brief handle sending an error and incoming error messages
 * 
 * Note: there should not be any incoming errors messages!
 * 
 * @param err code for error
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_error(uint16_t err, struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    auto msg = chat::error_msg(err);
    int len = sock.sendto(
        reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
        (sockaddr*)&client_address, sizeof(struct sockaddr_in));
}

/**
 * @brief handle broadcast message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/


void handle_broadcast(
    online_users& online_users, std::string username, std::string msg, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
     DEBUG("Received broadcast\n");

    // send message to all users, except the one we received it from
    for (const auto user: online_users) {
        DEBUG("username %s\n", user.first.c_str());
        if (strcmp(inet_ntoa(client_address.sin_addr), inet_ntoa(user.second->sin_addr)) == 0 &&
            client_address.sin_port != user.second->sin_port) {
            // send BC
            auto m = chat::broadcast_msg(username, msg);
            int len = sock.sendto(
                reinterpret_cast<const char*>(&m), sizeof(chat::chat_message), 0,
                (sockaddr*)user.second, sizeof(struct sockaddr_in));
        }
        else {
            DEBUG("Not sending message to self: %s\n", msg.c_str());
        }
    }
}

/**
 * @brief handle list message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_list(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    DEBUG("Received list\n");

    int username_size = MAX_USERNAME_LENGTH;
    int message_size  = MAX_MESSAGE_LENGTH;

    char username_data[MAX_USERNAME_LENGTH] = { '\0' };
    char * username_ptr = &username_data[0];
    char message_data[MAX_MESSAGE_LENGTH] = { '\0' };
    char * message_ptr = &message_data[0];

    bool using_username = true;
    bool full = false;

    for (const auto user: online_users) {
        if (using_username) {
            if (username_size - (user.first.length()+1) >= 0) {
                memcpy(username_ptr, user.first.c_str(), user.first.length());
                *(username_ptr+user.first.length()) = ':';
                username_ptr = username_ptr+user.first.length()+1;
                username_size = username_size - (user.first.length()+1);
                username_data[MAX_USERNAME_LENGTH - username_size] = '\0';
            }
            else {
                using_username = false;
            }
        }

        // otherwise we fill the message field
        if(!using_username) {
            if (message_size - (user.first.length()+1) >= 0) {
                memcpy(message_ptr, user.first.c_str(), user.first.length());
                *(message_ptr+user.first.length()) = ':';
                message_ptr = message_ptr+user.first.length()+1;
                message_size = message_size - (user.first.length()+1);
            }
            else {
                // we are full and we need to send packet and start again
                chat::chat_message msg{chat::LIST, '\0', '\0'};
                username_data[MAX_USERNAME_LENGTH - username_size] = '\0';
                memcpy(msg.username_, &username_data[0], MAX_USERNAME_LENGTH - username_size );
                message_data[MAX_MESSAGE_LENGTH - message_size] = '\0';
                memcpy(msg.message_, &message_data[0], MAX_MESSAGE_LENGTH - message_size );

                // 
                if (username.compare("__ALL") == 0) {
                    send_all(msg, "__ALL", online_users, sock);
                }
                else {
                    int len = sock.sendto(
                        reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                        (sockaddr*)&client_address, sizeof(struct sockaddr_in));
                }

                username_size = MAX_USERNAME_LENGTH;
                message_size  = MAX_MESSAGE_LENGTH;

                username_ptr = &username_data[0];
                message_ptr = &message_data[0];

                using_username = false;
            }
        }
    }

    if (using_username) {
        if (username_size >= 4) { 
            // enough space to store end in username
            memcpy(&username_data[MAX_USERNAME_LENGTH - username_size], USER_END, strlen(USER_END) );
            username_size = username_size - (strlen(USER_END)+1);
        }
        else {
            username_size = username_size + 1; // this enables overwriting the last ':'
            using_username = false;
        }
    }
    
    if (!using_username) {

    }

    chat::chat_message msg{chat::LIST, '\0', '\0'};
    username_data[MAX_USERNAME_LENGTH - username_size] = '\0';
    DEBUG("username_data = %s\n", username_data);
    memcpy(msg.username_, &username_data[0], MAX_USERNAME_LENGTH - username_size );
    message_data[MAX_MESSAGE_LENGTH - message_size] = '\0';
    memcpy(msg.message_, &message_data[0], MAX_MESSAGE_LENGTH - message_size );

    if (username.compare("__ALL") == 0) {
        send_all(msg, "__ALL", online_users, sock);
    }
    else {
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            (sockaddr*)&client_address, sizeof(struct sockaddr_in));
    }
}


/**
 * @brief handle join messageß
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/

void handle_join(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    DEBUG("Received join\n");

    // Check if the user is already online
    if (online_users.find(username) != online_users.end()) {
        // If the user is already online, send an error message
        handle_error(ERR_USER_ALREADY_ONLINE, client_address, sock, exit_loop);
    }
    else {
        // Add the new user to the map of online users
        online_users.emplace(username, new sockaddr_in(client_address));

        // Debug statement to confirm user addition
        DEBUG("User %s has joined\n", username.c_str());

        // Send back a JACK message to the client that has joined
        auto msg = chat::jack_msg();
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            (sockaddr*)&client_address, sizeof(struct sockaddr_in));

        // Debug statement to confirm JACK message sent
        DEBUG("Sent JACK message to %s\n", username.c_str());

        // Send a broadcast message to all other clients
        handle_broadcast(online_users, username, "has joined the server", "", client_address, sock, exit_loop);

        // Debug statement to confirm broadcast message sent
        DEBUG("Broadcast message sent from %s\n", username.c_str());

        // Send a list message to the new user
        handle_list(online_users, "__ALL", "", "", client_address, sock, exit_loop);

        // Debug statement to confirm list message sent
        DEBUG("List message sent to %s\n", username.c_str());
    }
}




/**
 * @brief handle jack message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_jack(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    DEBUG("Received jack\n");
    handle_error(ERR_UNEXPECTED_MSG, client_address, sock, exit_loop);
}

/**
 * @brief handle direct message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/


void handle_directmessage(
    online_users& online_users, std::string username, std::string message, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    // Retrieve sender's username using the client address
    std::string sender_username;
    for (const auto& user : online_users) {
        if (memcmp(&client_address, user.second, sizeof(struct sockaddr_in)) == 0) {
            sender_username = user.first;
            break;
        }
    }


    // Check if the sender's username was found
    if (sender_username.empty()) {
        // Handle error: sender's username not found
        DEBUG("Sender's username not found for client address\n");
        return;
    }

    DEBUG("Received direct message to %s from %s\n", sender_username.c_str(), username.c_str());

    // Handle sending direct message
    if (auto search = online_users.find(username); search != online_users.end()) {
        DEBUG("Found user\n");
        // Retrieve the sender's socket address
        sockaddr_in* recipient_addr = search->second;
        // Print the message being sent
        DEBUG("Sending direct message from %s to %s: %s\n", sender_username.c_str(), username.c_str(), message.c_str());
        // Send the message to the sender
        auto msg = chat::dm_msg(sender_username, message); // Pass sender's username as the first parameter
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            reinterpret_cast<sockaddr*>(recipient_addr), sizeof(struct sockaddr_in));
        // Check if message sent successfully
        if (len == sizeof(chat::chat_message)) {
            DEBUG("Direct message sent to %s\n", username.c_str());
        } else {
            // Handle error if message sending failed
            DEBUG("Failed to send direct message to %s\n", username.c_str());
            // TODO: Handle error message
        }
    } else {
        // TODO: Handle error message
        DEBUG("User not found\n");
    }
}






/**
 * @brief handle leave message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_leave(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    DEBUG("Received leave\n");

    username = "";
    // find username
    for (const auto user: online_users) {
        if (strcmp(inet_ntoa(client_address.sin_addr), inet_ntoa(user.second->sin_addr)) == 0 &&
            client_address.sin_port == user.second->sin_port) {
                username = user.first;
        }
    }
    DEBUG("%s is leaving the server\n", username.c_str());

    if (username.length() == 0) {
        // this should never happen
        handle_error(ERR_UNKNOWN_USERNAME, client_address, sock, exit_loop); 
    }
    else if (auto search = online_users.find(username); search != online_users.end()) {
        // first free memory for sockaddr
        struct sockaddr_in * addr = search->second;
        delete addr;

        // now delete from username map
        online_users.erase(search);

        // finally send back LACK
        auto msg = chat::lack_msg();
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            (sockaddr*)&client_address, sizeof(struct sockaddr_in));

        handle_broadcast(online_users, username, "has left the server", "", client_address, sock, exit_loop);
        msg = chat::chat_message{chat::LEAVE, '\0', '\0'};
        memcpy(msg.username_, username.c_str(), username.length()+1);
        send_all(msg, username, online_users, sock, false);
    }
    else {
        handle_error(ERR_UNKNOWN_USERNAME, client_address, sock, exit_loop); 
    }
}

/**
 * @brief handle lack message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_lack(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    DEBUG("Received lack\n");
    handle_error(ERR_UNEXPECTED_MSG, client_address, sock, exit_loop);
}

/**
 * @brief handle exit message
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/

// void handle_exit(
//     online_users& online_users, std::string username, std::string, 
//     struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
//     DEBUG("Received exit\n");

//     // send exit message (chat::exit_msg()) to each user, and clear up memory for them
    
    
//     // leave this code as it is required for exiting
//     exit_loop = true;
// }

void handle_exit(
    online_users& online_users, std::string username, std::string, std::string,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    DEBUG("Received exit\n");

    // send exit message (chat::exit_msg()) to each user, and clear up memory for them
    for (auto& pair : online_users) {
        auto msg = chat::exit_msg();
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            (sockaddr*)(pair.second), sizeof(struct sockaddr_in));
        
        // Delete memory allocated for sockaddr
        delete pair.second;
    }

    // Clear the map of online users
    online_users.clear();

    // Set exit_loop to true to terminate the event loop
    exit_loop = true;
}


/**
 * @brief
 * 
 * @param online_users map of usernames to their corresponding IP:PORT address
 * @param username part of chat protocol packet
 * @param msg part of chat protocol packet
 * @param client_address address of client to send message to
 * @param sock socket for communicting with client
 * @param exit_loop set to true if event loop is to terminate
*/
void handle_error(
    online_users& online_users, std::string username, std::string, std::string, 
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
     DEBUG("Received error\n");
}

/**
 * @brief function table, mapping command type to handler.
*/
void (*handle_messages[14])(online_users&, std::string, std::string, std::string, struct sockaddr_in&, uwe::socket&, bool& exit_loop) = {
    handle_join, handle_jack, handle_broadcast, handle_directmessage,
    handle_list, handle_leave, handle_lack, handle_exit, handle_group_creation, handle_user_addition_to_group, handle_user_removal_from_group,
    handle_group_message, handle_user_leave_group, handle_error
    
};

/**
 * @brief server for chat protocol
*/
void server() {
    // keep track of online users
    online_users online_users;

    // port to start the server on

	// socket address used for the server
	struct sockaddr_in server_address;
	memset(&server_address, 0, sizeof(server_address));
	server_address.sin_family = AF_INET;

	// htons: host to network short: transforms a value in host byte
	// ordering format to a short value in network byte ordering format
	server_address.sin_port = htons(SERVER_PORT);

	// htons: host to network long: same as htons but to long
	// server_address.sin_addr.s_addr = htonl(INADDR_ANY);
	// creates binary representation of server name and stores it as sin_addr
	inet_pton(AF_INET, uwe::get_ipaddr().c_str(), &server_address.sin_addr);

    // create a UDP socket
	uwe::socket sock{AF_INET, SOCK_DGRAM, 0};

	sock.bind((struct sockaddr *)&server_address, sizeof(server_address));

	// socket address used to store client address
	struct sockaddr_in client_address;
	size_t client_address_len = 0;

	char buffer[sizeof(chat::chat_message)];
    DEBUG("Entering server loop\n");
    bool exit_loop = false;
	for (;!exit_loop;) {
        int len = sock.recvfrom(
			buffer, sizeof(buffer), 0, (struct sockaddr *)&client_address, &client_address_len);

      
        DEBUG("Received chat_message:\n");
        if (len == sizeof(chat::chat_message)) {
            // handle incoming packet
            chat::chat_message * message = reinterpret_cast<chat::chat_message*>(buffer);
            auto type = static_cast<chat::chat_type>(message->type_);
            std::string username{(const char*)&message->username_[0]};
            std::string msg{(const char*)&message->message_[0]};
            std::string group_name{(const char*)&message->group_name_[0]};
            DEBUG("Type %d\n", type);

            if (is_valid_type(type)) {
                DEBUG("handling msg type %d\n", type);
                // valid type, so dispatch message handler
                handle_messages[type](online_users,username, msg, group_name, client_address, sock, exit_loop);
            }
            else{
                DEBUG("Type not valid");
            }
        }
        else {
            DEBUG("Unexpected packet length\n");
        }
    }
}

/**
 * @brief entry point for chat server application
*/
int main(void) { 
    // Set server IP address
    uwe::set_ipaddr("192.168.1.6");
    server();

    return 0;
}