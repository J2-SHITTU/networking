#WORKSHEET 2

For worksheet 2 part 2, we were told to clone a git repository that has the the code files needed for this part

#TASK 1

I cloned the repository, and then we were asked to implement some of the missing **_chat_server.cpp_** functionality. Specifically the **_handle_join_**, _**handle_directmessage**_, and **_handle_exit_** functions. 
    
**handle_join implementation**:
```c++
    void handle_join(
        online_users& online_users, std::string username, std::string,
        struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
        DEBUG("Received join\n");

        // Check if the user is already online
        if (online_users.find(username) != online_users.end()) {
            // If the user is already online, send an error message
                handle_error(ERR_USER_ALREADY_ONLINE, client_address, sock, exit_loop);
        }
        else {
            // Add the new user to the map of online users
            online_users.emplace(username, new sockaddr_in(client_address));

            // Debug statement to confirm user addition
            DEBUG("User %s has joined\n", username.c_str());

            // Send back a JACK message to the client that has joined
            auto msg = chat::jack_msg();
            int len = sock.sendto(
                reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                (sockaddr*)&client_address, sizeof(struct sockaddr_in));

            // Debug statement to confirm JACK message sent
            DEBUG("Sent JACK message to %s\n", username.c_str());

            // Send a broadcast message to all other clients
            handle_broadcast(online_users, username, "has joined the server", client_address, sock, exit_loop);

            // Debug statement to confirm broadcast message sent
            DEBUG("Broadcast message sent from %s\n", username.c_str());

            // Send a list message to the new user
            handle_list(online_users, "__ALL", "", client_address, sock, exit_loop);

            // Debug statement to confirm list message sent
            DEBUG("List message sent to %s\n", username.c_str());
        }
    }
```
**Debugging:** The function starts with a debug statement indicating that a join message has been received.

**Check if User is Already Online:** It checks if the user trying to join is already present in the list of online users. If the user is already online, it sends an error message using the handle_error function.

**Add User to Online Users:** If the user is not already online, it adds the new user to the map of online users. It also sends a **JACK** message back to the client that has joined, confirming their successful connection to the server.

**Broadcast Message:** Next, it sends a broadcast message to all other clients informing them that a new user has joined the server. This message typically includes the username of the new user along with a notification that they have joined.

**List Message:** Finally, it sends a list message to the new user, providing them with information about all the other users currently online. This allows the new user to see who else is present in the chat.

**Other Debugging Statements:** Throughout the function, there are debug statements to confirm each step of the process, such as user addition, message sending, and list message confirmation.

Overall, this function will manage the process of handling a user joining the chat server, update the list of online users, send appropriate messages to notify other users, and provide necessary information to the new user.

**handle_directmessage implementation:**
```c++
void handle_directmessage(
    online_users& online_users, std::string username, std::string message,
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    DEBUG("Received directmessage to %s\n", username.c_str());

    // Handle sending direct message
    if (auto search = online_users.find(username); search != online_users.end()) {
        DEBUG("Found user\n");
        // Iterate through online users to find the recipient
        for (const auto& user : online_users) {
            // Convert client IP addresses to strings for comparison
            std::string client_ip_str = inet_ntoa(client_address.sin_addr);
            std::string user_ip_str = inet_ntoa(user.second->sin_addr);
            // Compare client IP addresses using strcmp
            if (strcmp(client_ip_str.c_str(), user_ip_str.c_str()) == 0) {
                // Print the message being sent
                DEBUG("Sending direct message to %s: %s\n", username.c_str(), message.c_str());
                // Send the message to the user if the client IP addresses match
                auto msg = chat::dm_msg(username, message);
                int len = sock.sendto(
                    reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
                    (sockaddr*)(user.second), sizeof(struct sockaddr_in));
                // Check if message sent successfully
                if (len == sizeof(chat::chat_message)) {
                    DEBUG("Direct message sent to %s\n", username.c_str());
                } else {
                    // Handle error if message sending failed
                    DEBUG("Failed to send direct message to %s\n", username.c_str());
                    // TODO: Handle error message
                }
            }
        }
    } else {
        // TODO: Handle error message
        DEBUG("User not found\n");
    }
}
```

**Debugging:** It starts with a debug statement indicating that a direct message has been received for a specific user.

**Find User:** It attempts to find the recipient user in the **online_users** map. If the recipient is found, it proceeds to send the direct message to them.

**Iterate Through Online Users:** It iterates through all online users to find the recipient. For each user, it compares the client IP address of the sender with the IP address of the user in the map.

**Send Direct Message:** If the IP addresses match, it constructs a direct message using the **chat::dm_msg** function and sends it to the recipient using the sock.sendto function.

**Other Debugging Statements:** Throughout the function, there are debugging statements to confirm various steps of the process, including finding the user, sending the direct message, and handling errors.

**Error Handling:** If the recipient user is not found in the **online_users** map, it prints a debug statement indicating that the user was not found. It may also include a TODO comment to handle error messages more effectively in the future.

This function will manage the process of handling direct messages between users in the chat server, findthe recipient user, sending the message, and handle errors if the user is not found or if the message sending fails.

**Handle_exit**
```c++
void handle_exit(
    online_users& online_users, std::string username, std::string, 
    struct sockaddr_in& client_address, uwe::socket& sock, bool& exit_loop) {
    
    DEBUG("Received exit\n");

    // send exit message (chat::exit_msg()) to each user, and clear up memory for them
    for (auto& pair : online_users) {
        auto msg = chat::exit_msg();
        int len = sock.sendto(
            reinterpret_cast<const char*>(&msg), sizeof(chat::chat_message), 0,
            (sockaddr*)(pair.second), sizeof(struct sockaddr_in));
        
        // Delete memory allocated for sockaddr
        delete pair.second;
    }

    // Clear the map of online users
    online_users.clear();

    // Set exit_loop to true to terminate the event loop
    exit_loop = true;
}
```
**Debugging:** It starts with a debug statement indicating that an exit message has been received for a specific user.

**Sending Exit Messages:** It iterates through all online users in the **online_users** map and sends an exit message to each user. This is done using the **chat::exit_msg()** function to construct the exit message and the **sock.sendto** function to send it.

**Clearing Memory:** For each user, it also deletes the memory allocated for the **sockaddr_in** structure associated with that user.

**Clearing Online Users:** After sending exit messages to all users, it clears the online_users map to remove all users from the chat server.

**Setting Exit Loop Flag:** Finally, it sets the **exit_loop** flag to true, indicating that the event loop should be terminated.

This function will ensure that all users are notified of the exit of a specific user, clear the memory associated with those users, remove all users from the chat server, and signal the termination of the event loop.

After that, I added some bits of code to the **chat_client.cpp** codefile. Specifically the switch statements for the client side. Originally it was this
```c++
switch(type) {
                            case chat::EXIT: {
                                DEBUG("Received Exit from GUI\n");
                                // you need to fill in
                                break;
                            }
                            case chat::LEAVE: {
                                DEBUG("Received LEAVE from GUI\n");
                                // you need to fill in
                                break;
                            }
                            case chat::LIST: {
                                DEBUG("Received LIST from GUI\n");
                                // you need to fill in
                                break;
                            }
                            default: {
                                // the default case is that the command is a username for DM
                                // <username> : message
                                if (cmds.size() == 2) {
                                    DEBUG("Received message from GUI\n");
                                }
                                break;
                            }
                        } 

```
That was just printing DEBUG statemnts in the client side's GUI whenever those commands were used. So, I added code to make the output more reasonable

```c++
switch(type) {
                        case chat::EXIT: {
                            DEBUG("Received Exit from GUI\n");
                            // Prepare exit message and send it to the server
                            chat::chat_message exit_msg = chat::exit_msg();
                            int len = sock.sendto(
                            reinterpret_cast<const char*>(&exit_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            exit_loop = true; // Set exit flag to true to break out of the loop
                            break;
                        }
                        case chat::LEAVE: {
                            // Prepare leave message and send it to the server
                            chat::chat_message leave_msg = chat::leave_msg();
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&leave_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            exit_loop = true; // Set sent_leave flag to true
                            break;
                        }
                        case chat::LIST: {
                            DEBUG("Received LIST from GUI\n");
                            // Prepare list request message and send it to the server
                            chat::chat_message list_msg = chat::list_msg(username);
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&list_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            break;
                        }
                        default: {
                        // Handle message for direct message (DM) if the command is not recognized
                            if (cmds.size() == 2) {
                                // Extract recipient username and message from the command
                                std::string recipient = cmds[0]; // Assuming the first part of the command is the recipient
                                std::string message = cmds[1];
                                // Prepare direct message and send it to the server
                                chat::chat_message dm_msg = chat::dm_msg(recipient, message);
                                int len = sock.sendto(
                                reinterpret_cast<const char*>(&dm_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }
                            break;
                        }
                    }
```
**chat::EXIT:** it prepares an exit message (exit_msg) using the **chat::exit_msg()** function. Then, it sends this exit message to the server using the **sock.sendto** function. Additionally, it sets the **exit_loop** flag to true, indicating that the program should exit the loop and terminate.

**chat::LEAVE:** it prepares a leave message (leave_msg) using the **chat::leave_msg()** function. Similar to the exit case, it sends this leave message to the server and sets the **exit_loop** flag to true.

**chat::LIST:** it prepares a list request message (list_msg) using the **chat::list_msg()** function. It then sends this list request message to the server.

**Default Case**: If the received command is not recognized as exit, leave, or list, it assumes it's a direct message (DM). It extracts the recipient username and message from the command and prepares a direct message (dm_msg) using the **chat::dm_msg()** function. Finally, it sends this direct message to the server.

After the implementaion of the server side functions and addition of code to the client side, I tested the program, and the output showed that the functions were working as expected. 

I ran
```bash
/opt/iot/bin/iotdump --ip --udp --udp_header --udp_msg --chat -f packets/iotpacket_192.168.1.7_8867
```
to display the **ip_adddress**, **udp information**, and most importantly, the **type** of messages being sent to the server. 

This was the output:

**Screenshot of mesage type, and other information sent to server**
![client_to_server](<screenshots/Screenshot 2024-03-16 at 16.11.21.png>)
_This screenshot displays  the **ip_adddress**, **udp information**, and the **type** of messages being sent from the client to the server._

**Note:** This server has an ip_address of "192.168.1.7" and not "192.168.1.8" because the given server ip_adddress "192.168.1.8" was not working at this time of completion of this task

Then I ran 
```bash
/opt/iot/bin/iotdump --ip --udp --udp_header --udp_msg --chat -f packets/iotpacket_192.168.1.11_1111
```
to display what is being sent to the client and this was th output:

**Screenshot of message type, and other information sent to client**
![server_to_client](<screenshots/Screenshot 2024-03-16 at 16.28.32.png>)
_This screenshot displays  the **ip_adddress**, **udp information**, and the **type** of messages being sent from the server to the client._


**TASK 2**

The client has two threads: one for handling messages from the user interface (UI) and another for handling messages from the server. These threads are necessary to ensure that messages from one source do not block the processing of messages from the other source, and we were asked to create two channels—one for communication from the UI thread to the main thread (gui_tx and gui_rx) and another for communication from the server to the main thread (rec_rx).

For this we had to complete this function
```c++
std::pair<std::thread, Channel<chat::chat_message>> make_receiver(uwe::socket* sock) {
  auto [tx, rx] = make_channel<chat::chat_message>();
  
  std::thread receiver_thread{[](Channel<chat::chat_message> tx, uwe::socket* sock) { 
    try {
        for (;;) {
            chat::chat_message msg;
            
            // you need to fill in
            // receive message from server
            // send it over channel (tx) to main UI thread
            
            // exit receiver thread
            if (msg.type_ == chat::EXIT || (msg.type_ == chat::LACK && sent_leave)) {
                break;
            }
        }
    }
    catch(...) {
        DEBUG("caught exception\n");
    };
  }, std::move(tx), sock};

  return {std::move(receiver_thread), std::move(rx)};
}

```
and this was implementaion
```c++
std::pair<std::thread, Channel<chat::chat_message>> make_receiver(uwe::socket* sock) {
    auto [tx, rx] = make_channel<chat::chat_message>();
    
    std::thread receiver_thread{[](Channel<chat::chat_message> tx, uwe::socket* sock) { 
        try {
            for (;;) {
                chat::chat_message msg;
                
                // Receive message from server
                int bytes_received = sock->recvfrom(reinterpret_cast<char*>(&msg), sizeof(chat::chat_message), 0, nullptr, nullptr);
                
                if (bytes_received > 0) {
                    // Send the received message over the channel (tx) to the main UI thread
                    tx.send(msg);
                }    
                // Exit receiver thread if an exit message is received from the server
                if (msg.type_ == chat::EXIT || (msg.type_ == chat::LEAVE && sent_leave)) {
                    break;
                }
            }
        }
        catch(...) {
            DEBUG("caught exception\n");
        }
    }, std::move(tx), sock};

    return {std::move(receiver_thread), std::move(rx)};
}
```

Inside the lambda function, there's a try block where the receiver thread continuously loops to receive messages from the socket. It uses recvfrom to receive a message and sends it through the channel tx. The loop breaks if an exit message (chat::EXIT) is received or if a leave message (chat::LEAVE) is received and the sent_leave flag is set. If an exception occurs during the message receiving process, it's caught and a debug message is printed.

In other to prevent the DEBUG statements from printing on th UI, when running the "chat_client" program, we were asked to run it this way **_/opt/iot/bin/chat_client "ipaddress" "port" "username" 2> debug.log_**

Where **debug.log** is a file the debug statements will be redirected to. 

When I ran the program and viwd the statemnts in a new terminal using this
```bash
tail -f debug.log
```
I got this 

**Screenshot of debug statements to client side**
![debug_messages](<screenshots/Screenshot 2024-03-17 at 12.58.28.png>)
_This screenshot shows the debug statments being sent to the client side._

While this is running, the UI is also updating  
**Screenshot of first user join**
![first_user](<screenshots/Screenshot 2024-03-17 at 18.17.39.png>)
_This screenshot shows the first user joined._

**Screenshot of second user join**
![second_user](<screenshots/Screenshot 2024-03-17 at 18.18.35.png>)
_This screenshot shows the broadcast message indicating a second user has joined._

**Screenshots of direct message**
**first_user_pov**
![first_user_pov](<screenshots/Screenshot 2024-03-30 at 18.22.11.png>)

**second_user_pov**
![second_user_pov](<screenshots/Screenshot 2024-03-30 at 18.22.34.png>)
_These screenshots shows UI of both the sender and the receiver after a direct message is sent._

**Screenshots of exit command**

**server side**
![server_side](<screenshots/Screenshot 2024-03-17 at 21.30.19.png>)

**client_side**
![client_side](<screenshots/Screenshot 2024-03-17 at 21.34.39.png>)

_These screenshots shows the aftermath of running the exit command. All loops close._

**TASK 3**

The third task involved us extending our code to support groups. For this I had to make changes to the **chat_client.cpp**, **chat_server.cpp**, and the **chat.hpp** header file found in this directory.
```bash
/opt/iot/bin/include
```
This task was a bit extensive, and to make sure I cover all what I did, I am going to be splitting my explanation into three parts

_**PART 1 (chat.hpp)**_ 
After copying the chat.hpp header file into my directory, I extended the **chat_type** to support more types.
```c++
enum chat_type {
    JOIN = 0,
    JACK,
    BROADCAST,
    DIRECTMESSAGE,
    LIST,
    LEAVE,
    LACK,
    EXIT,
    GROUP_CREATE,        // Create a new group
    GROUP_ADD_USER,      // Add a user to a group
    GROUP_REMOVE_USER,   // Remove a user from a group
    GROUP_MESSAGE,       // Send a message to a group
    GROUP_LEAVE,
    ERROR,
    UNKNOWN,
};
```
as you can see, I added chat types related to the commands I wanted users to be able to do whenever their request regarded groups. 
I also added a new variable to support group names, "group_name_" here in the **chat_message** struct 
```c++
struct chat_message {
    uint8_t type_;
    int8_t username_[MAX_USERNAME_LENGTH];
    int8_t message_[MAX_MESSAGE_LENGTH];
    int8_t group_name_[MAX_USERNAME_LENGTH];
};
```
Then I implemented functions for each of them.

**GROUP_CREATE**
```C++
// Function to create a group
inline chat_message create_group_msg(std::string group_name) {
    chat_message msg{GROUP_CREATE, '\0'};
    memcpy(&msg.group_name_[0], group_name.c_str(), group_name.length());
    msg.group_name_[group_name.length()] = '\0';
    return msg;
}
```
Takes the **group_name** as an argument, and copies the characters using **memcpy** into a variable **group_name_** for use later on. Then returns the object of **chat_message **associated with the **GROUP_CREATE **chat type, **'msg'**

**GROUP_ADD_USER**
```c++
// Function to add a user to a group
inline chat::chat_message add_user_to_group_msg(std::string group_name, std::string username) {
    chat::chat_message msg{chat::GROUP_ADD_USER, '\0', '\0'};
    memcpy(&msg.username_[0], username.c_str(), username.length());
    msg.username_[username.length()] = '\0';
    memcpy(&msg.group_name_[0], group_name.c_str(), group_name.length());
    msg.group_name_[group_name.length()] = '\0';
    return msg;
}
```
Takes the **group_name**  and **username **as arguments, and copies the characters using **memcpy** into a variables **group_name_** and **username_** respectively, for use later on. Then returns the object of **chat_message **associated with the **GROUP_ADD_USER **chat type, **'msg'**

**GROUP_REMOVE_USER**
```c++
// Function to remove a user from a group
inline chat::chat_message remove_user_from_group_msg(std::string group_name, std::string username) {
    chat::chat_message msg{chat::GROUP_REMOVE_USER, '\0', '\0'};
    memcpy(&msg.username_[0], username.c_str(), username.length());
    msg.username_[username.length()] = '\0';
    memcpy(&msg.group_name_[0], group_name.c_str(), group_name.length());
    msg.group_name_[group_name.length()] = '\0';
    return msg;
}
Takes the **group_name**  and **username **as arguments, and copies the characters using **memcpy** into a variables **group_name_** and **username_** respectively, for use later on. Then returns the object of **chat_message **associated with the **GROUP_REMOVE_USER **chat type, **'msg'**
```

**GROUP_MESSAGE**
```C++
// Function to send a message to a group
inline chat::chat_message group_msg(std::string group_name, std::string username, std::string message) {
    chat::chat_message msg{chat::GROUP_MESSAGE, '\0', '\0', '\0'};
    memcpy(&msg.username_[0], username.c_str(), username.length());
    msg.username_[username.length()] = '\0';
    memcpy(&msg.group_name_[0], group_name.c_str(), group_name.length());
    msg.group_name_[group_name.length()] = '\0';
    memcpy(&msg.message_[0], message.c_str(), message.length());
    msg.message_[message.length()] = '\0';
    return msg;
}
```
Takes the **group_name**, **username **, and **message** as arguments, and copies the characters using **memcpy** into a variables **group_name_**, **username_** , and **message_** respectively, for use later on. Then returns the object of **chat_message **associated with the **GROUP_MESSAGE **chat type, **'msg'**

**GROUP_LEAVE**
```c++
inline chat::chat_message leave_group_msg(std::string group_name, std::string username) {
    chat::chat_message msg{chat::GROUP_LEAVE, '\0', '\0'};
    memcpy(&msg.group_name_[0], group_name.c_str(), group_name.length());
    msg.group_name_[group_name.length()] = '\0';
    memcpy(&msg.username_[0], username.c_str(), username.length());
    msg.username_[username.length()] = '\0';
    return msg;
}
```
Takes the **group_name**  and **username **as arguments, and copies the characters using **memcpy** into a variables **group_name_** and **username_** respectively, for use later on. Then returns the object of **chat_message **associated with the **GROUP_LEAVE **chat type, **'msg'**

_**PART 2 (chat_client.cpp)**_
In the client side of things, first I added more cases to the **to_type** function
```c++
chat::chat_type to_type(std::string cmd) {
  switch(string_to_int(cmd.c_str())) {
    // case string_to_int("join"): return chat::JOIN;
    // case string_to_int("bc"): return chat::BROADCAST;
    // case string_to_int("dm"): return chat::DIRECTMESSAGE;
    case string_to_int("list"): return chat::LIST;
    case string_to_int("leave"): return chat::LEAVE;
    case string_to_int("exit"): return chat::EXIT;
    case string_to_int("creategroup"): return chat::GROUP_CREATE;
    case string_to_int("addusertogroup"): return chat::GROUP_ADD_USER;
    case string_to_int("rmuserfromgroup"): return chat::GROUP_REMOVE_USER;
    case string_to_int("leavegroup"): return chat::GROUP_LEAVE;
    case string_to_int("groupmsg"): return chat::GROUP_MESSAGE;
    default:
      return chat::UNKNOWN; 
  }

  return chat::UNKNOWN; // unknowntype
}
```
added these additional cases to introduce functionality for managing groups within the chat application. Users who choose to, can create groups, add or remove users from groups, send messages to groups, and leave groups with these commands.
Obviously they would not have done anything because I had not specified what to do when those commands are inputted.
To do that I added this pieces of code to the codefile

```c++
case chat::GROUP_CREATE: {
                            DEBUG("Received group_create from GUI\n");
                            // Extract group name from the command
                            if (cmds.size() == 2 && cmds[0] == "creategroup") {
                            std::string group_name = cmds[1];
                            DEBUG("Group name: %s\n", group_name.c_str());
                            // Create a message to create the group
                            chat::chat_message create_group_msg = chat::create_group_msg(group_name);

                            // Send the message to the server
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&create_group_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }
                            break;  
}   

```

This checks if the size of the input seprated by columns is 2, and the string before the column is **"create_group"** ,and if it is, it should create a message to create the group using the create_group_msg function implemented in the chat.hpp file, and passing in the first string after the column which serves as the group name into it. Then it sends the message to the server.

```c++                
                        
                        case chat::GROUP_ADD_USER: {
                            // Extract group name and username from the command
                            if (cmds.size() == 3 && cmds[0] == "addusertogroup") {
                            std::string group_name = cmds[1];
                            std::string username = cmds[2];
                            // Create a message to add user to the group
                            chat::chat_message add_user_to_group_msg = chat::add_user_to_group_msg(group_name, username);
                            // Send the message to the server
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&add_user_to_group_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }
                            break;                           
                        }
```

This checks if the size of the input seprated by columns is 3, and the string before the first column is **"addusertogroup"** ,and if it is, it should create a message to add the user using the add_user_to_group_msg function implemented in the chat.hpp file, and passing in the first string after the first column which serves as the group name , and the first string after the second column which serves as the username. Then it sends the message to the server.

```c++
                        case chat::GROUP_REMOVE_USER: {
                            // Extract group name and username from the command
                            if (cmds.size() == 3 && cmds[0] == "rmuserfromgroup") {
                            std::string group_name = cmds[1];
                            std::string username = cmds[2];
                            // Create a message to add user to the group
                            chat::chat_message remove_user_from_group_msg = chat::remove_user_from_group_msg(group_name, username);
                            // Send the message to the server
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&remove_user_from_group_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }                        
                            break;
                        }
```

This checks if the size of the input seprated by columns is 3, and the string before the first column is **"remvoeuserfromgroup"** ,and if it is, it should create a message to remove the user using the remove_user_from_group_msg function implemented in the chat.hpp file, and passing in the first string after the first column which serves as the group name , and the first string after the second column which serves as the username. Then it sends the message to the server.

```c++
                        case chat::GROUP_LEAVE: {
                            // Extract group name and username from the command
                            if (cmds.size() == 2 && cmds[0] == "leavegroup") {
                            std::string group_name = cmds[1];
                            // Create a message to add user to the group
                            chat::chat_message leave_group_msg = chat::leave_group_msg(group_name, "");
                            // Send the message to the server
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&leave_group_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }
                            break;
                        
                        }
```
This checks if the size of the input seprated by columns is 2, and the string before the first column is **"leavegroup"** ,and if it is, it should create a message to remove the user using the leave_group_msg function implemented in the chat.hpp file, and passing in the first string after the first column into it, which serves as the group name. Then it sends the message to the server.

```c++
                        case chat::GROUP_MESSAGE: {
                            // Extract group name and username from the command
                            if (cmds.size() == 3 && cmds[0] == "groupmsg") {
                            std::string group_name = cmds[1];
                            std::string username = "";
                            std::string message= cmds[2];
                            // Create a message to add user to the group
                            chat::chat_message group_msg = chat::group_msg(group_name, username, message);
                            // Send the message to the server
                            int len = sock.sendto(
                                reinterpret_cast<const char*>(&group_msg), sizeof(chat::chat_message), 0,
                                (sockaddr*)&server_address, sizeof(server_address));
                            }

                            break;  
                        }
```
This checks if the size of the input seprated by columns is 3, and the string before the first column is **"groupmsg"** ,and if it is, it should create a message to send a message to the group specified using the group_msg function implemented in the chat.hpp file, and passing in the first string after the first column which serves as the group name, and the first string after the second column which serves as the message into it. Then it sends the message to the server.

After sending the desired message from the client, it should ideally receive one back from the server. If it does, th message should be displayed appropriate on the client's user interface(UI). 
For this, I added code to format the messages received in a suitable manner.

```c++
case chat::GROUP_CREATE: {
                        std::string msg{"Group"};
                        msg.append(" '");
                        msg.append((char*)(*result).group_name_);
                        msg.append("' ");
                        msg.append("created");                   
                        chat::display_command cmd{chat::GUI_CONSOLE, msg};
                        gui_tx.send(cmd);
                        break;
                    }
```
This is supposed to display the string "Group '{group_name}' created" on the UI. The placeholder would have the value passed into the variable. I put it in a curly bracket for explanation

```C++
                    case chat::GROUP_MESSAGE: {
                        std::string msg{"Group message("};
                        msg.append((char*)(*result).group_name_);
                        msg.append("):");
                        msg.append((char*)(*result).username_);
                        msg.append(": ");
                        msg.append((char*)(*result).message_)                                                 
                        chat::display_command cmd{chat::GUI_CONSOLE, msg};
                        gui_tx.send(cmd);
                        break;
                    }
```
This is supposed to display the string "Group message({group_name_}):{username_}: {message_} '{group_name}' created" on the UI   

```c++

                    case chat::GROUP_ADD_USER: {
                        std::string msg{"You have been added to group '"};
                        msg.append((char*)(*result).group_name_); 
                        msg.append("'");
                        chat::display_command cmd{chat::GUI_CONSOLE, msg};
                        gui_tx.send(cmd);
                        break;
                    }
```
This is supposed to display the string " You have been added to group '{group_name_}'" on the UI 

```c++

                    case chat::GROUP_REMOVE_USER: {
                        std::string msg{""};
                        msg.append((char*)(*result).username_);
                        msg.append(" removed you from group '");
                        msg.append((char*)(*result).group_name_);
                        msg.append("'");
                        chat::display_command cmd{chat::GUI_CONSOLE, msg};
                        gui_tx.send(cmd);
                        break;
                    }
```
This is supposed to display the string "{username_} removed you from group '{group_name_}'" on the UI 

```c++
                    case chat::GROUP_LEAVE: {
                        std::string msg{""};
                        msg.append((char*)(*result).username_);
                        msg.append(" left group '");
                        msg.append((char*)(*result).group_name_); 
                        msg.append("'");
                        chat::display_command cmd{chat::GUI_CONSOLE, msg};
                        gui_tx.send(cmd);
                        break;
                    }
```
This is supposed to display the string "{username_}' left group '{group_name_}'" on the UI 

